# Lima Innovation Lab

## Instrucciones

### Construir imagenes
docker-compose -f local.yml build

### Realizar migraciones en la bd Postgres
docker-compose -f local.yml run --rm --service-ports django python manage.py makemigrations
docker-compose -f local.yml run --rm --service-ports django python manage.py migrate

### Crear super usuario (para usar el admin)
docker-compose -f local.yml run --rm --service-ports django python manage.py createsuperuser

### Levantar proyecto
docker-compose -f local.yml up

### Agregar datos
Podemos agregar datos con el admin de django
Necesitamos para las pruebas al menos una solicitud y un usuario

### Acceder a los endpoints
GET localhost:8000/solicitud/           listar todas las solicitudes
GET localhost:8000/solicitud/<id>/      obtener una solicitud
PUT localhost:8000/solicitud/<id>/      actualizar una solicitud

POST localhost:8000/users/login/        iniciar sesion