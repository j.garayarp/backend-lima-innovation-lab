# Models
from lil.approval_requests.models import Request

# Django REST Framework
from rest_framework import serializers

class RequestModelSerializer(serializers.ModelSerializer):
    """Request model serializer."""

    class Meta:
        """Meta class."""

        model = Request
        fields = (
            'id',
            'nombre_credito',
            'monto_credito',
            'monto_sbs',
            'puntuacion_sentinel',
            'indicador_ia'
        )