"""Requests views."""

# Django REST Framework
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

# Models
from lil.approval_requests.models import Request

# Serializers
from lil.approval_requests.serializers import RequestModelSerializer


class RequestViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """Request view set."""

    queryset = Request.objects.all()
    serializer_class = RequestModelSerializer