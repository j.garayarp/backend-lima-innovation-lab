"""User models admin."""

# Django
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Models
from lil.approval_requests.models import Request


@admin.register(Request)
class SolicitudesAdmin(admin.ModelAdmin):
    """Solicitud model admin."""

    list_display = ('monto_credito', 'monto_sbs', 'puntuacion_sentinel', 'indicador_ia')