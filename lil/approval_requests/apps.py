"""Requests app."""

# Django
from django.apps import AppConfig


class RequestsAppConfig(AppConfig):
    """Requests app config."""

    name = 'lil.approval_requests'
    verbose_name = 'Requests'
