"""Requests models."""

# Django
from django.db import models

# Utilities
from lil.utils.models import LilModel


class Request(LilModel):
    """Request model."""

    nombre_credito = models.CharField(max_length=255, default='credito')
    monto_credito = models.FloatField()
    monto_sbs = models.FloatField()
    puntuacion_sentinel = models.CharField(max_length=10) # bueno, regular o malo
    indicador_ia = models.IntegerField() # 1 al 10

    def __str__(self):
        return '{}'.format(self.nombre_credito)

    class Meta:
        ordering = ['pk']