"""Rides URLs."""

# Django
from django.urls import include, path

# Django REST Framework
from rest_framework.routers import DefaultRouter

# Views
from .views import requests as request_views

router = DefaultRouter()
router.register(
    r'solicitud',
    request_views.RequestViewSet,
    basename='solicitud'
)

urlpatterns = [
    path('', include(router.urls))
]