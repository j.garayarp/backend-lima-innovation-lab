"""User model."""

# Django
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

# Utilities
from lil.utils.models import LilModel


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, first_name=None, last_name=None, **kwargs):
        if not email:
            raise ValueError('Ingresa un email')
        if not first_name:
            raise ValueError('Ingresa tu nombre')
        if not last_name:
            raise ValueError('Ingresa tu apellido')
        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, **kwargs)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name, last_name):
        user = self.create_user(email, password=password, first_name=first_name, last_name=last_name)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class User(LilModel, AbstractUser):
    """User model.

    Extend from Django's Abstract User, change the username field
    to email and add some extra fields.
    """

    email = models.EmailField(
        'email address',
        unique=True,
        error_messages={
            'unique': 'Ya existe un usuario con este correo.'
        }
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        """Return first_name and last_name."""
        return '{} {}'.format(self.first_name, self.last_name)

    def get_short_name(self):
        """Return first_name and last_name."""
        return '{} {}'.format(self.first_name, self.last_name)